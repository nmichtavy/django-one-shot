from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm
# Create your views here.


def todo_list(request):
    todo = TodoList.objects.all()
    context = {
        "todo_list": todo
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todos = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todos
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            detail = form.save()
            return redirect("todo_list_detail", detail.id)
    else:
        form = TodoListForm()

        context = {
            "form": form
        }
        return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todos = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todos)
        if form.is_valid():
            detail = form.save()
            return redirect("todo_list_detail", detail.id)
    else:
        form = TodoListForm(instance=todos)

        context = {
            "todos": todos,
            "form": form,
        }
    return render(request, "todos/edit.html", context)


def delete_list(request, id):
    delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoItemForm()

        context = {
            "form": form
        }
        return render(request, "todos/item.html", context)


def todo_item_update(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            detail = form.save()
            return redirect("todo_list_detail", detail.id)
    else:
        form = TodoItemForm(instance=item)

        context = {
            "form": form,
        }
    return render(request, "todos/update.html", context)
